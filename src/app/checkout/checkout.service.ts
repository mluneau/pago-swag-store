import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { v4 as uuid } from 'uuid';
import { Observable } from 'rxjs';
import { nanoid } from "nanoid";

@Injectable({
  providedIn: 'root'
})
export class CheckoutService {

  constructor(private http: HttpClient) { }

  private  transaction_gateway_url = "https://transactiongateway.dev.pagoservices.com";

  checkoutProducts(
    pago_account_ids: String[],
    total_price,
    asset,
  ): Observable<Object> {
    const transaction_id = uuid();
    const individualAmount = Number((Number(total_price) / pago_account_ids.length).toPrecision(asset.decimals));
    console.log(individualAmount);
    const transactions: Object = {};
    pago_account_ids.forEach((pago_account_id) => {
      const transaction_interface_id = uuid();
      transactions[transaction_interface_id] =  {
        type: 'TransferRequest',
        sender: pago_account_id,
        fee: 1000,
        amount: individualAmount,
        receiver: 'pagostore$pagoservices.com',
        lease: nanoid(32),
        assetId: asset.assetId,
        agreementRequest: {
          id: transaction_id,
          type: 'text',
          description: 'Pago Store receipt',
          body: 'contents of receipt'
        }
      };
    });
    if (pago_account_ids.length === 1) {
      return this.http.post(`${this.transaction_gateway_url}/transactions/envelopes`, {
        name: transaction_id,
        description: 'Payment from Pago Store',
        sender: pago_account_ids[0],
        applicationId: 'pago_store',
        transactionInterfaceRequest: transactions[Object.keys(transactions)[0]],
      });
    } else {
      return this.http.post(`${this.transaction_gateway_url}/transactions/envelopes`, {
        name: transaction_id,
        description: 'Payment from Pago Store',
        sender: pago_account_ids[0],
        applicationId: 'pago_store',
        transactionInterfaceRequest: {
          type: 'GroupTransactionRequest',
          fee: 1000,
          agreementRequest: {
            id: transaction_id,
            type: 'text',
            description: 'Pago Store receipt',
            body: 'contents of receipt'
          },
          transactionInterfaceMapRequest: transactions,
        }
      });
    }
  }
}
