export interface IAsset {
    assetId: string;
    unitName: string;
    decimals: number;
}
