import { Injectable } from '@angular/core';
import { IProduct } from '../products/product';

@Injectable({
  providedIn: 'root'
})
export class CartService {

  products: IProduct[] = new Array();

  constructor() { }

  getProducts(): IProduct[] {
    return this.products;
  }

  addProduct(product: IProduct): void {
    this.products.push(product);
  }

  getPrice() {
    if (this.products.length === 0) {
      return 0;
    }
    return this.products
              .map(product => product.price)
              .reduce((sum, current) => sum + current);
  }

  clearProducts(): void {
    this.products = new Array();
  }
}
