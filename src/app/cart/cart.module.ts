import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CartComponent } from './cart.component';
import { SharedModule } from '../shared/shared.module';
import { CheckoutModule } from '../checkout/checkout.module';

@NgModule({
  imports: [
    RouterModule.forChild([
      { path: 'cart', component: CartComponent },
    ]),
    SharedModule,
    CheckoutModule
  ],
  declarations: [
    CartComponent,
  ]
})
export class CartModule { }
